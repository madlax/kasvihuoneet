'use strict';

/* App Module */

angular.module('kasvihuone', ['kasvihuoneFilters', 'kasvihuoneServices']).
	config(['$routeProvider', function($routeProvider) {
		$routeProvider.
			when('/kasvihuoneet', {templateUrl: 'partials/kasvihuone-list.html',   controller: KasvihuoneListCtrl}).
			when('/kasvihuoneet/tiedot/:kasvihuoneId', {templateUrl: 'partials/kasvihuone-detail.html', controller: KasvihuoneDetailCtrl}).
			when('/kasvihuoneet/esittely/:huoneid',{templateUrl: 'partials/kasvihuone-list.html', controller: KasvihuoneListCtrl}).
			when('/kasvihuoneet/huoneet',{templateUrl: 'partials/kasvihuone-huoneet.html', controller: KasvihuoneHuoneetCtrl}).
			when('/kasvihuoneet/yhteys',{templateUrl: 'partials/kasvihuone-yhteys.html', controller: KasvihuoneYhteysCtrl}).
		otherwise({redirectTo: '/kasvihuoneet'});
}]);
