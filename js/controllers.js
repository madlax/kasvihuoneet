'use strict';

/* Controllers */
function HtmlCtrl($scope, Kasvihuone, Page) {
		$scope.Page = Page;
}
function HeadCtrl($scope, Kasvihuone, Linkki) {
		//Linkki.sharedObject.windowTitle = "Kas";
}
function MainCtrl($scope, Kasvihuone, $http, $location,$routeParams,$route, Linkki, Page) {

	$scope.avaaHuone2 = function() {
		Linkki.sharedObject.huoneNavi = false;
	}	
}

function KasvihuoneListCtrl($scope, Kasvihuone, $http, $location,$routeParams,$route, Linkki, Page) {

	var huoneid = $routeParams.huoneid;
	if (huoneid===undefined){
		huoneid = 0;
	}
	
	//estää hypyn sivun ylälaitaan linkkiä klikattaessa blokkaamalla reitityksen
	var lastRoute = $route.current;
	Linkki.sharedObject.huoneNavi = true;
	$scope.$on('$locationChangeSuccess', function(event) {
		//blokkaa sivulataus jos klikattiin huonenavigaatiota 
		if(Linkki.sharedObject.huoneNavi){
			$route.current = lastRoute;
		}

	});		
	
	//haetaan kasvihuoneen esittelytiedot
	$http.get('kasvihuoneet/kasvihuoneet.json').success(function(data) {
		$scope.huoneet = data;
		$scope.mainImg = data[huoneid].imageUrl;
		$scope.huoneNimi = data[huoneid].name;
		$scope.huoneKuvaus = data[huoneid].kuvaus;
		$scope.huoneUrl = '#/kasvihuoneet/tiedot/'+data[huoneid].id;
		$scope.selected = data[huoneid].id;
	});  
	
	//lajitellaan kasvihuoneet listausjärjestkyksen mukaan
	$scope.kasvihuoneet = Kasvihuone.query();
	$scope.orderProp = 'age';
	
	//taulukossa klikatun kasvihuoneen speksit
	$scope.setImage = function(kasvihuone) {
		//kasvihuone.preventDefault();
		$scope.mainImg = kasvihuone.imageUrl;
		$scope.huoneNimi = kasvihuone.name;
		$scope.huoneKuvaus = kasvihuone.kuvaus;
		$scope.huoneUrl = '#/kasvihuoneet/tiedot/'+kasvihuone.id;
		$scope.selected = kasvihuone.id; //lisätään selected class
		$location.path('/kasvihuoneet/esittely/'+kasvihuone.age);
		Page.setTitle('Kasvihuoneet | '+ $scope.huoneNimi);
	}
	
	$scope.avaaHuone = function() {
		
		Linkki.sharedObject.huoneNavi = false;
	}	
		
}

//KasvihuoneListCtrl.$inject = ['$scope', 'Kasvihuone'];
function KasvihuoneHuoneetCtrl($scope, Kasvihuone,$rootScope,  $routeParams,$http, Page) {
	Page.setTitle('Kasvihuoneet listaus');
	
	//lajitellaan kasvihuoneet listausjärjestkyksen mukaan
	$scope.kasvihuoneet = Kasvihuone.query();
	$scope.orderProp = 'age';
	
	$scope.moro = function(huone){
		$scope.jou = Kasvihuone.get({kasvihuoneId: huone});	
		return $scope.jou;
		
	}	

}


function KasvihuoneDetailCtrl($scope, $routeParams, Kasvihuone, Page) {
	
	$scope.kasvihuone = Kasvihuone.get({kasvihuoneId: $routeParams.kasvihuoneId}, function(kasvihuone) {
		$scope.mainImageUrl = kasvihuone.images[0];
		Page.setTitle(kasvihuone.name + ' tiedot');
	});

  $scope.setImage = function(imageUrl) {
    $scope.mainImageUrl = imageUrl;
  }
}

function KasvihuoneYhteysCtrl($scope, $routeParams, Kasvihuone, Page) {
	
	Page.setTitle('Kasvihuoneet - ota yhteyttä');
}

