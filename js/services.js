'use strict';

/* Services */

angular.module('kasvihuoneServices', ['ngResource']).
factory('Kasvihuone', function($resource){
	return $resource('kasvihuoneet/:kasvihuoneId.json', {}, {
		query: {method:'GET', params:{kasvihuoneId:'kasvihuoneet'}, isArray:true}
  });
})
.factory('Linkki', function(){
  return {sharedObject: {huoneNavi: false, windowTitle: 'asdsad' } }
})
.factory('iidee', function(){
  return {sharedObject: {iitee: 'asd' } }
})
.factory('Page', function(){
  var title = 'Kasvihuoneet';
  return {
    title: function() { return title; },
    setTitle: function(newTitle) { title = newTitle; }
  };
});